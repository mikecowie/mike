pdf:
	mkdir docs || true
	echo "CV - Mike Cowie - 2019" > docs/professional.md
	echo "=====" >> docs/professional.md
	cat site/professional.md | sed '1d;2d;3d;4d;5d;6d;' >> docs/professional.md
	docker run --rm -v $(CURDIR):/app jmaupetit/md2pdf --css=doc.css docs/professional.md docs/cv.pdf
