---
layout: page
title: About
permalink: /about/
---

I am a 28 year old (as of 2019) IT professional and football referee from Christchurch, New Zealand, living in Christchurch, New Zealand.

Professional
---

I have worked in IT for 4 years, and I'm keen on Linux, containers, and automation. I guess I'd say I'm keen on "devops" and "cloud" type stuff, but I'm reasonably aware of how those terms are somewhat "buzzwords."
I foresee a lot more coding and developing in my life.

In a former life, I have a science background with a Bachelor's degree majoring in Biochemistry and Chemistry. I had a punt at secondary teaching a while back, but decided it wasn't for me.

Other interests
---

I've been a football referee since I was 16, and I've gained many friends - as well as maintained good fitness- through this.I'm also a sometime keen runner with a couple of marathons under  my belt - I feel like number 3 is a long way away though.
