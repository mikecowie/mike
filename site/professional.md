---
layout: page
title:  "CV - Mike Cowie - 2019"
date:   2019-04-13 21:14:00 +1200
categories: professional
---

Education and Certification
-----

* Bachelor of Science, Biochemistry and Chemistry, University of Canterbury, 2009-2011
* Diploma of Network Engineering, Computer Power Plus, 2015-2016
* Red Hat:
    * Certified Systems Administrator, 2017
    * Certified Engineer, 2018
* AWS:
   * Solutions Architect (Associate), 2019

Experience
-----

* Linux and Windows System Administration
* General scripting:
    * Bash
    * Powershell
    * Python
* Infrastructure-as-code and configuration mangement:
    * CI/CD principles and workflows
    * Ansible
    * Terraform
    * CloudFormation
    * Docker administration and application deployment
    * Kubernetes administration and application deployment
* Core application components:
    * Databases ( MySQL/MariaDB, Postgresql, Elasticsearch, MongoDB )
    * Webservers ( Nginx, Apache )
    * Monitoring and alerting tools
    * Active Directory, LDAP, IPA
* Cloud and hosting platforms:
    * AWS
    * Azure
    * GCE
    * VMWare

Characteristics
-----

* Systematic, analytic and outcome-driven approach.
* Technically proficient and fast to learn.
* Clear written and verbal communication.
* Security-focused.
* Knowledgeable about current trends and issues in the industry.
* Team-focused.
* Bad puns.

Work History
-----

* 2018-present - Technical Analyst, Computer Concepts Ltd, Christchurch
* 2016-2018 - Service Desk Engineer, Computer Concepts Ltd, Christchurch
* 2013, 2015-2016 - Online Tutor, yourtutor.com.au
* 2012-2013 - English Teacher, Global Education Centre, Incheon, Republic of Korea
* 2010-2011 - Library Project Assistant, University of Canterbury, Christchurch (part-time)
