---
layout: post
title:  "Deploy AKS Kubernetes and core services"
date:   2019-04-14 15:00:00 +1200
categories: kubernetes
---

This project comes out of my trying to learn kubernetes, and in particular, trying to build an understanding of how to manage core security requirements - user access limits, monitoring, alerting, and TLS termination.

It also encompassed learning a lot about Azure and how it interfaces with Kubernetes with their AKS service.

You can check out my deployment and attempt to follow along from the source at [here](https://gitlab.com/mikecowie/terraform-azurerm-aks)

### Prerequisites

* You have got terraform 11.* installed
* You have got a close-to-current version of the Azure CLI installed, or else you're on your own about finding another way to authenticate to Azure for testing purposes.
* You have an azure subscription available with enough access to create AAD applications and deploy almost any resources.
* `kubectl` command-line tool.

### Choices

I've want to build out an opinionated deployment which can take advantage of the best of what my platform might have to offer, but doesn't lock me down to the platform. Fundamentally the only thing I'm going to need to be locked to is Kubernetes.

Therefore I've chosen tools that, at their core, solve problems in the most Kubernetes-native way possible.

### Monitoring

For monitoring based on metrics, by far the best "native" Kubernetes tool is prometheus. Kubernetes exposes a huge amount of data, via its own authetication mechanisms, for prometheus to process. Sitting on top of prometheus is alertmanager for sending/managing the lifecycle of alerts, and I would be considering using Grafana for better visualization/dashboards.

### Log aggregation

Google's fluentd is the best "native" way to pick up logs from Kubernetes pods. From there, in my view, its a platform-specific question what fluentd should do with them - maybe send to Azure monitor for Azure, Stackdriver for Google, Elasticsearch for AWS or anywhere else where we're running ELK stack, or to Splunk/Graylog/Datadog if that's what we've invested in. My demonstration includes a rudimentary in-cluster ELK stack for demonstration purposes.

### TLS termination

Kubernetes "ingress" resource is the basis of how end-users connect to HTTP(S) applications hosted in Kubernetes. I've chosen the nginx ingress controller as it simply seems to be the best ingress controller - of course Traefik or perhaps HAProxy would be others, and there are also platform-specific ingress controllers like the AWS ALB ingress controller.

Complementing the nginx ingress controller is cert-manager for managing certificates, which I'll use to pick up letsencrypt certificates for my services.

## Example applications

2 fairly simple example applications are included - wordpress and cacti. They're both simple LAMP stack applications with a bit of persistent storage required.

## Network Policy

I've desired and attempted to attempt Network Policy, but Azure's support for it seems spotty at best. When I attempted it, the "calico" pods that drive it refused to run and blocked my worker nodes from entering service.

## Get started - Deploy the cluster

Checkout the code from git and change into the directory
~~~
# git clone https://gitlab.com/mikecowie/terraform-azurerm-aks
# cd  terrafirn-azurerm-aks
~~~

Login to azure (for production deployments through CI/CD process you'd need to create an AzureAD application, role, and credentials to inject into the runner's environment)

~~~
# az login
~~~

Deploy the cluster

~~~
terraform plan
terraform apply
~~~

Follow the output - in 5-10 minutes you'll have a perfectly functional 1-node aks cluster. When it's complete, use the following command to set up your kubectl configuration

~~~
az aks get-credentials --resource-group aks-test --name aks-test
~~~

Now you can check your worker node(s) is available.

~~~
kubectl get nodes
~~~

As well as deploying AKS, terraform also created some nicely templated manifests for you to deploy nginx ingress controllers for internal access (e.g. inside a corporate network) as well as external access (e.g from the internet). Lets deploy them with kubectl.

~~~
kubectl apply -f tmp/ingress-internal/
kubectl apply -f tmp/ingress-external/
~~~

And certmanager - one of the custom resources for this requires --validate=false to deploy

~~~
kubectl apply -f certmanager/
kubectl apply --validate=false -f certmanager/
~~~

At this point, our ingress-external loadbalancer should have been created by azure. To be able to get letsencrypt certificates for it, we need a valid domain that resolves over public DNS. If we have a public DNS zone that we can manage for ourselves, we just need to create a wildcard A record for the domain. For example, create a wildcard A record that matches the external IP adress output in

~~~
"kubectl get services --namespace=operations-ingress-external" ingress-nginx"
~~~

In our case, we have a test domain kub.cclcloudtest.concepts.co.nz for this purpose in AWS Route 53, so we can create an A record for \*.kub.cclcloudtest.concepts.co.nz . If you don't have your own DNS zone you can use \*.A.B.C.D.nip.io , where A.B.C.D is your ingress controller's external IP address

Deploy prometheus, and when it's available, port-forward to one of the pods to see the prometheus and alert-manager web interfaces. I haven't yet added ingress resources for these!
~~~
kubectl apply -f monitoring/namespaces.yml
kubectl apply -f monitoring/prometheus/
kubectl apply -f monitoring/alertmanager/
kubetl get pods --namespace=operations-monitoring

## When available, prometheus
kubectl port-forward --namespace=operations-monitoring prometheus-<tab-complete to find pod name> 9090:9090
# Browse localhost:9090 in your browser .

## When avaialble, alertmanager
kubectl port-forward --namespace=operations-monitoring alertmanager-<tab-complete to find pod name> 9093:9093
# Browse localhost:9093 in your browser .
~~~

Deploy fluentd and Elastic Stack. It's available, port-forward to the kibana web interfaces. I haven't yet added ingress resources for this!

~~~
kubectl apply -f logging/namespaces.yml
kubectl apply -f logging/fluentd/
kubectl apply -f logging/elastic
kubetl get pods --namespace=operations-logging

## When available, kibana
kubectl port-forward --namespace=operations-logging kibana-<tab-complete to find pod name> 5601:5601
# Browse localhost:5601 in your browser .

~~~

Great! Logging, monitoring and alerting tools installed  to play with, pulling in data from all across our Kubernetes cluster.

Now for some example applications.

Edit the files `samples/wordpress/wordpress-ingress.yml` and `samples/cacti/ingress.yml` to reflect your domain, or else you'll not be able to connect to these over the internet, and certmanager won't be able to get letsencrypt certificates for them.

~~~
kubectl apply -f samples/cacti/
kubectl apply -f samples/wordpress/
~~~

These deployments will provision Azure managed disks to attach as persistent Volumes, and once they go through initiation, will be accessible at wordpress.yourdomain.com and cacti.yourdomain.com . The current deployment uses the letsencrypt staging CA for testing purposes which your browser won't trust, but check out the annotations on your ingress resouces in `samples/cacti/ingress.yml` and `samples/wordpress/wordpress-ingress.yml` - - if you're pushing on towards production you can switch to the "production" letsencrypt provider and get a real "Green padlock".

If you are deploying the application internally, rather than via the internet, you'd need a few adjustments. If you have control over your how your internal DNS domain is advertised on the internet, you can use Letsencrypt's DNS solver to get Letencrypt certificates for internal sites.
Otherwise you'll want to check out some of certmanager's other providers.

When you're done playing, shut everything down to save cost.

~~~
terraform destroy
~~~
