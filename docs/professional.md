CV - Mike Cowie - 2019
=====

Education and Certification
-----

* Bachelor of Science, Biochemistry and Chemistry, University of Canterbury, 2009-2011
* Diploma of Network Engineering, Computer Power Plus, 2015-2016
* Red Hat:
    * Certified Systems Administrator, 2017
    * Certified Engineer, 2017
* AWS:
   * Solutions Architect (Associate), 2019

Experience
-----

* Linux and Windows System Administration
* General scripting:
    * Bash
    * Powershell
    * Python
* Infrastructure-as-code and configuration mangement:
    * CI/CD principles and workflows
    * Ansible
    * Terraform
    * CloudFormation
    * Docker administration and application deployment
    * Kubernetes administration and application deployment
* Core application components:
    * Databases ( Especially MySQL/MariaDB, Postgresql )
    * Webservers ( Nginx, Apache )
    * Monitoring and alerting (Prometheus ecosystem, PRTG)
    * Identity Services (Active Directory and open-source identity services, and integrating web-native and cloud-native services)
* Cloud and hosting platforms:
    * AWS
    * Azure
    * GCE
    * VMWare
    * DigitalOcean
* Storage:
    * Filesystems and volume managers - strong working knowledge of LVM, ZFS and friends
    * Storage services - experience with SMB/Samba, NFS, and S3 implementations

Characteristics
-----

* Systematic, analytic and outcome-driven approach.
* Technically proficient and fast to learn.
* Clear written and verbal communication.
* Security-focused.
* Knowledgeable about current trends and issues in the industry.
* Team-focused.

Work History
-----

* 2018-present - Technical Analyst, Computer Concepts Ltd, Christchurch
* 2016-2018 - Service Desk Engineer, Computer Concepts Ltd, Christchurch
* 2013, 2015-2016 - Online Tutor, yourtutor.com.au
* 2012-2013 - English Teacher, Global Education Centre, Incheon, Republic of Korea
* 2010-2011 - Library Project Assistant, University of Canterbury, Christchurch (part-time)
